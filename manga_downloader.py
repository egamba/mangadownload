# Imports
import requests
import sys
import urlparse
import os
import tqdm
from lxml import html

# Argument parsing
manganame   = str(sys.argv[1])
curdir      = str(sys.argv[2])
low         = int(sys.argv[3])


# change the directory you want
# curdir='/Users/crunchmonster/Documents/MangaDownloader/'

if not os.path.exists(curdir + manganame):
    os.makedirs(curdir + manganame)

weburl  = 'http://www.mangapanda.com/'+manganame+'/'
test = True
max = low
if len(sys.argv) < 5 :
	print 'Checking for available chapters ...'
	while test:
	    curweburl = weburl + str(max) + '/'
	    response = requests.get( curweburl + '1')
	    parsed_body = html.fromstring(response.text)
	        
	        # Grab links to all images
	    images = parsed_body.xpath('//img/@src')
	    if not images:
	        condition = False
	        
	    # Convert any relative urls to absolute urls
	    images = [urlparse.urljoin(response.url, url) for url in images]
	    z = url.split('/'[-1])


	    if ((manganame in z)==False):
	        break
	    max = max+1

	print 'There are '+str(max)+ ' chapters available !'
	
else : 
	max = int(sys.argv[4])

print 'Downloading ' + manganame + ' starting from chapter ' + str(low) + ' to chapter ' + str(max)
for dir in tqdm.tqdm(range(low,max)):

    curweburl = weburl + str(dir) + '/'
    namedir = 'chapter'+str(dir)
    if not os.path.exists(curdir + manganame+'/'+namedir):
        os.makedirs(curdir+manganame+'/'+namedir)

    condition = True
    i= 1
    while condition:
        response = requests.get( curweburl + str(i))
        parsed_body = html.fromstring(response.text)
        
        # Grab links to all images
        images = parsed_body.xpath('//img/@src')
        if not images:
            condition = False

        # Convert any relative urls to absolute urls
        images = [urlparse.urljoin(response.url, url) for url in images]
        z = url.split('/'[-1])
        if ((manganame in z)==False):
            sys.exit()
        r = requests.get(url)
        f = open(curdir + manganame+'/'+namedir+'/'+str(i)+'.jpg','w')
        f.write(r.content)
        f.close()
        i = i+1


#ajouter les elements pour verifier les dernieres version des mangas
