import requests  
from lxml import html  
import sys  
import urlparse
import os
from progressbar import ProgressBar
import tqdm


comicname = str(sys.argv[1])
j=0
low = 1
if len(sys.argv) > 2:
    low = int(sys.argv[2])
    j = low-1
if len(sys.argv) > 3:
    high = int(sys.argv[3])


print 'Downloading ' + comicname + ' starting from chapter ' + str(low)
curdir='/Users/crunchmonster/Documents/ComicsDownloader/'

if not os.path.exists(curdir + comicname):
    os.makedirs(curdir + comicname)

sum = 0
MainCondition = True
while  MainCondition:
    j=j+1
    weburl  = 'http://www.readcomics.net/'+comicname +'/'
    curweburl = weburl + 'chapter-'+str(j) + '/full'
    namedir = 'chapitre'+str(j)
    if not os.path.exists(curdir + comicname+'/'+namedir):
        os.makedirs(curdir+comicname+'/'+namedir)

    condition = True
    i= 1
    print comicname + ' : downloading chapter [' + str(j)+']'

    while condition:
        pbar = ProgressBar()
        response = requests.get( curweburl ) ## replace with the website you want
        parsed_body = html.fromstring(response.text)
        # Grab links to all images
        images = parsed_body.xpath('//img/@src')
        if not images:
            condition = False

        # Convert any relative urls to absolute urls
        images = [urlparse.urljoin(response.url, url) for url in images]
        # Only download first x 10
        for url in tqdm.tqdm(images):## 100 will scrape 100 pictures
            r = requests.get(url)
            z = url.split('/'[-1])
            if (('404.png' in z)==True):
                MainCondition=False
            name = curdir + comicname+'/'+namedir+'/'+str(i)+'.jpg'
            f = open(name,'w')
            f.write(r.content)
            f.close()
            i=i+1

        condition = False